const neatURL = require("./index")

describe('neatURL', () => {  

	it("Remove utm_source from querystring", () => {
		expect(neatURL({
			url: "https://www.example.com/bens-tagging?utm_source=mysite.com&utm_medium=referral&utm_campaign=url+tracking+post#Echobox=1568564590",
			includeHash: true
		})).toBe("https://www.example.com/bens-tagging");
	});

	it("Remove Echobox hash variable", () => {
		expect(neatURL({
			url: "https://nationalpost.com/news/world/in-edward-snowdens-new-memoir-the-disclosures-this-time-are-personal#Echobox=1568564590",
			includeHash: true
		})).toBe("https://nationalpost.com/news/world/in-edward-snowdens-new-memoir-the-disclosures-this-time-are-personal");
	});

	it("Ignore hash key with no value", () => {
		expect(neatURL({
			url: "https://nationalpost.com/news/world/in-edward-snowdens-new-memoir-the-disclosures-this-time-are-personal#abcdefg",
			includeHash: true
		})).toBe("https://nationalpost.com/news/world/in-edward-snowdens-new-memoir-the-disclosures-this-time-are-personal#abcdefg");
	});

	it("Remove s query from twitter links", () => {
		expect(neatURL({
			url: "https://twitter.com/SpongeBob/status/1167430538388488192?s=21"
		})).toBe("https://twitter.com/SpongeBob/status/1167430538388488192");
	});

	it("Remove emc and partner from nytimes links", () => {
		expect(neatURL({
			url: "https://www.nytimes.com/2019/09/15/health/vaping-thc-wisconsin.html?emc=rss&partner=rss"
		})).toBe("https://www.nytimes.com/2019/09/15/health/vaping-thc-wisconsin.html");
	});

	it("Remove ref_src and ref_url from twitter.com", () => {
		expect(neatURL({
			url: "https://twitter.com/abc/status/123?ref_src=twsrc%5Etfw%7Ctwcamp%5Etweetembed%7Ctwterm%5E123456&ref_url=https%3A%2F%2Fwww.example.com"
		})).toBe("https://twitter.com/abc/status/123");
	});

	it("Remove amp from query", () => {
		expect(neatURL({
			url: "https://www.example.com/news/123/abc?amp=&__twitter_impression=true"
		})).toBe("https://www.example.com/news/123/abc");
	});
	
	it("Remove Amazon params", () => {
		expect(neatURL({
			url: "https://www.amazon.com/gp/product/123/ref=123?ie=UTF8&creative=123&linkCode=as2&creativeASIN=123&linkId=123"
		})).toBe("https://www.amazon.com/gp/product/123");
	});

	it("Normalize youtube url", () => {
		expect(neatURL({
			url: "https://www.youtube.com/watch?v=xxx&feature=push-prem-sub&attr_tag=xxx&ab_channel=xxx"
		})).toBe("https://www.youtube.com/watch?v=xxx");
	});

	it("Remove Webtrends campaign parameters", () => {
		expect(neatURL({
			url: "http://www.domain.com/landing.aspx?country=US&WT.mc_id=EmailCampaign1&WT.mc_ev=click&WT.srch=1"
		})).toBe("http://www.domain.com/landing.aspx?country=US");
	});

	it("Don't remove a trailing slash", () => {
		const url = 'http://doma.in/foo/?CNDID=bar';
		const expectedUrl = 'http://doma.in/foo/';
		expect(neatURL({url})).toBe(expectedUrl);
	});

	 it("Don't remove Echobox if it is used as anchor.", () => {
		const url = 'http://doma.in/foo/#Echobox-explained';
		const expectedUrl = 'http://doma.in/foo/#Echobox-explained';
		expect(neatURL({url, includeHash: true})).toBe(expectedUrl);
	});

	it("Don't remove other query strings when removing Echobox", () => {
		const url = 'http://doma.in/foo/#foo=bar&Echobox=1568564590&baz=quux';
		const expectedUrl = 'http://doma.in/foo/#foo=bar&baz=quux';
		expect(neatURL({url, includeHash: true})).toBe(expectedUrl);
	});

	[{
		hostname: 'www.amazon.com',
		url: 'https://www.amazon.com/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.com/foo/?bar=baz'
	}, {
		hostname: 'www.amazon.ca',
		url: 'https://www.amazon.ca/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.ca/foo/?bar=baz'
	}, {
		hostname: 'www.amazon.co.jp',
		url: 'https://www.amazon.co.jp/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.co.jp/foo/?bar=baz'
	}, {
		hostname: 'www.amazon.co.uk',
		url: 'https://www.amazon.co.uk/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.co.uk/foo/?bar=baz'
	}, {
		hostname: 'www.amazon.de',
		url: 'https://www.amazon.de/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.de/foo/?bar=baz'
	}, {
		hostname: 'www.amazon.es',
		url: 'https://www.amazon.es/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.es/foo/?bar=baz'
	}, {
		hostname: 'www.amazon.jp',
		url: 'https://www.amazon.jp/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://www.amazon.jp/foo/?bar=baz'
	}, {
		hostname: 'amazon.com',
		url: 'https://amazon.com/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.com/foo/?bar=baz'
	}, {
		hostname: 'amazon.ca',
		url: 'https://amazon.ca/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.ca/foo/?bar=baz'
	}, {
		hostname: 'amazon.co.jp',
		url: 'https://amazon.co.jp/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.co.jp/foo/?bar=baz'
	}, {
		hostname: 'amazon.co.uk',
		url: 'https://amazon.co.uk/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.co.uk/foo/?bar=baz'
	}, {
		hostname: 'amazon.de',
		url: 'https://amazon.de/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.de/foo/?bar=baz'
	}, {
		hostname: 'amazon.es',
		url: 'https://amazon.es/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.es/foo/?bar=baz'
	}, {
		hostname: 'amazon.jp',
		url: 'https://amazon.jp/foo/?bar=baz&utm_campaign=123&creativeASIN=123&_encoding=123',
		expected: 'https://amazon.jp/foo/?bar=baz'
	}, {
		hostname: 'google.de',
		url: 'https://google.de/foo/?bar=baz&utm_campaign=123&sei=123',
		expected: 'https://google.de/foo/?bar=baz'
	}, {
		hostname: 'www.google.de',
		url: 'https://www.google.de/foo/?bar=baz&utm_campaign=123&sei=123',
		expected: 'https://www.google.de/foo/?bar=baz'
	}, {
		hostname: 'bing.com',
		url: 'https://bing.com/foo/?bar=baz&utm_campaign=123&pq=123',
		expected: 'https://bing.com/foo/?bar=baz'
	}, {
		hostname: 'www.bing.com',
		url: 'https://www.bing.com/foo/?bar=baz&utm_campaign=123&pq=123',
		expected: 'https://www.bing.com/foo/?bar=baz'
	}, {
		hostname: 'youtube.com',
		url: 'https://youtube.com/foo/?bar=baz&utm_campaign=123&feature=123',
		expected: 'https://youtube.com/foo/?bar=baz'
	}, {
		hostname: 'www.youtube.com',
		url: 'https://www.youtube.com/foo/?bar=baz&utm_campaign=123&feature=123',
		expected: 'https://www.youtube.com/foo/?bar=baz'
	}, {
		hostname: 'reddit.com',
		url: 'https://reddit.com/foo/?bar=baz&utm_campaign=123&st=123',
		expected: 'https://reddit.com/foo/?bar=baz'
	}, {
		hostname: 'www.reddit.com',
		url: 'https://www.reddit.com/foo/?bar=baz&utm_campaign=123&st=123',
		expected: 'https://www.reddit.com/foo/?bar=baz'
	}, {
		hostname: 'twitter.com',
		url: 'https://twitter.com/foo/?bar=baz&utm_campaign=123&ref_src=123',
		expected: 'https://twitter.com/foo/?bar=baz'
	}, {
		hostname: 'www.twitter.com',
		url: 'https://www.twitter.com/foo/?bar=baz&utm_campaign=123&ref_src=123',
		expected: 'https://www.twitter.com/foo/?bar=baz'
	}, {
		hostname: 'nytimes.com',
		url: 'https://nytimes.com/foo/?bar=baz&utm_campaign=123&partner=123',
		expected: 'https://nytimes.com/foo/?bar=baz'
	}, {
		hostname: 'www.nytimes.com',
		url: 'https://www.nytimes.com/foo/?bar=baz&utm_campaign=123&partner=123',
		expected: 'https://www.nytimes.com/foo/?bar=baz'
	}, {
		hostname: 'instagram.com',
		url: 'https://instagram.com/foo/?bar=baz&utm_campaign=123&igshid=123',
		expected: 'https://instagram.com/foo/?bar=baz'
	}, {
		hostname: 'www.instagram.com',
		url: 'https://www.instagram.com/foo/?bar=baz&utm_campaign=123&igshid=123',
		expected: 'https://www.instagram.com/foo/?bar=baz'
	}, {
		hostname: 'studios.amazon.com',
		url: 'https://studios.amazon.com/?pf_rd_r=78572HY2XB8Z6Z8ZZ9X8&pf_rd_p=4022db67-4fe1-4a27-a354-1b62ba78e92b',
		expected: 'https://studios.amazon.com/'
	}, {
		hostname: 'www.amazon.co.uk',
		url: 'https://www.amazon.co.uk/product-reviews/B0038XBOJC?ie=UTF8&pd_rd_wg=ChPj3&pf_rd_p=ab0f8dce-7f41-4f1b-a486-fa30b5d29eaa',
		expected: 'https://www.amazon.co.uk/product-reviews/B0038XBOJC'
	}, {
		hostname: 'smile.amazon.com',
		url: 'https://smile.amazon.com/ref=sxts_snpl?pd_rd_w=GlNAO&pf_rd_p=64cfd66b-ee4a-4cad-934e-7c669aaca454&pf_rd_r=E9NPNDWGV07YNKYNBZZY&pd_rd_r=2d257574-57db-4a97-ba37-ca0b84be31f2&pd_rd_wg=A60bd',
		expected: 'https://smile.amazon.com/'
		
		
  }].forEach(({hostname, url, expected}) => {
		it(
			'Removes query paramaters specific to ' + hostname,
			() => expect(neatURL({url})).toBe(expected)
		)
	})

});

